package ru.pisarev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.pisarev.tm.exception.empty.EmptyIdException;
import ru.pisarev.tm.exception.empty.EmptyIndexException;
import ru.pisarev.tm.exception.empty.EmptyNameException;
import ru.pisarev.tm.exception.system.IndexIncorrectException;
import ru.pisarev.tm.marker.DBCategory;
import ru.pisarev.tm.dto.Project;

import java.util.List;

public class ProjectServiceTest {

    @Nullable
    private ProjectService projectService;

    @Nullable
    private Project project;

    @Before
    public void before() {
        projectService = new ProjectService(new ConnectionService(new PropertyService()));
        project = projectService.add("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", new Project("Project"));
    }

    @Test
    @Category(DBCategory.class)
    public void add() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        Assert.assertNotNull(project.getName());
        Assert.assertEquals("Project", project.getName());

        @NotNull final Project projectById = projectService.findById(project.getId());
        Assert.assertNotNull(projectById);
        Assert.assertEquals(project.getId(), projectById.getId());
    }

    @Test
    @Category(DBCategory.class)
    public void findAll() {
        @NotNull final List<Project> projects = projectService.findAll();
        Assert.assertTrue(projects.size() > 0);
    }

    @Test
    @Category(DBCategory.class)
    public void findAllByUserId() {
        @NotNull final List<Project> projects = projectService.findAll("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce");
        Assert.assertTrue(projects.size() > 0);
    }

    @Test
    @Category(DBCategory.class)
    public void findAllByUserIdIncorrect() {
        @NotNull final List<Project> projects = projectService.findAll("test");
        Assert.assertNotEquals(1, projects.size());
    }

    @Test
    @Category(DBCategory.class)
    public void findById() {
        @NotNull final Project project = projectService.findById("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", this.project.getId());
        Assert.assertNotNull(project);
    }

    @Test
    @Category(DBCategory.class)
    public void findByIdIncorrect() {
        @NotNull final Project project = projectService.findById("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", "34");
        Assert.assertNull(project);
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyIdException.class)
    public void findByIdNull() {
        @NotNull final Project project = projectService.findById("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", null);
        Assert.assertNull(project);
    }

    @Test
    @Category(DBCategory.class)
    public void findByIdIncorrectUser() {
        @NotNull final Project project = projectService.findById("test", this.project.getId());
        Assert.assertNull(project);
    }

    @Test
    @Category(DBCategory.class)
    public void remove() {
        projectService.removeById(project.getId());
        Assert.assertNull(projectService.findById(project.getId()));
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyIdException.class)
    public void testRemoveNull() {
        projectService.removeById(null);
    }

    @Test
    @Category(DBCategory.class)
    public void findByName() {
        @NotNull final Project project = projectService.findByName("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", "Project");
        Assert.assertNotNull(project);
    }

    @Test
    @Category(DBCategory.class)
    public void findByNameIncorrect() {
        @NotNull final Project project = projectService.findByName("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", "34");
        Assert.assertNull(project);
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyNameException.class)
    public void findByNameNull() {
        @NotNull final Project project = projectService.findByName("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", null);
        Assert.assertNull(project);
    }

    @Test
    @Category(DBCategory.class)
    public void findByNameIncorrectUser() {
        @NotNull final Project project = projectService.findByName("test", this.project.getName());
        Assert.assertNull(project);
    }

    @Test
    @Category(DBCategory.class)
    public void findByIndex() {
        @NotNull final Project project = projectService.findByIndex("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", 1);
        Assert.assertNotNull(project);
    }

    @Category(DBCategory.class)
    @Test(expected = IndexIncorrectException.class)
    public void findByIndexIncorrect() {
        @NotNull final Project project = projectService.findByIndex("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", 34);
        Assert.assertNull(project);
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyIndexException.class)
    public void findByIndexNull() {
        @NotNull final Project project = projectService.findByIndex("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", null);
        Assert.assertNull(project);
    }

    @Category(DBCategory.class)
    @Test(expected = IndexIncorrectException.class)
    public void findByIndexIncorrectUser() {
        @NotNull final Project project = projectService.findByIndex("test", 0);
        Assert.assertNull(project);
    }

    @Test
    @Category(DBCategory.class)
    public void removeById() {
        projectService.removeById("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", project.getId());
        Assert.assertNull(projectService.findById(project.getId()));
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyIdException.class)
    public void removeByIdNull() {
        projectService.removeById("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", null);
    }

    @Category(DBCategory.class)
    @Test(expected = IndexIncorrectException.class)
    public void removeByIndexIncorrect() {
        projectService.removeByIndex("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", 34);
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyIndexException.class)
    public void removeByIndexNull() {
        projectService.removeByIndex("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", null);
    }

    @Category(DBCategory.class)
    @Test(expected = IndexIncorrectException.class)
    public void removeByIndexIncorrectUser() {
        projectService.removeByIndex("test", 0);
    }


    @Category(DBCategory.class)
    @Test(expected = EmptyNameException.class)
    public void removeByNameNull() {
        projectService.removeByName("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", null);
    }


}
