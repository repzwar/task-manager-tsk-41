package ru.pisarev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.pisarev.tm.exception.empty.EmptyIdException;
import ru.pisarev.tm.exception.empty.EmptyIndexException;
import ru.pisarev.tm.exception.empty.EmptyNameException;
import ru.pisarev.tm.marker.DBCategory;
import ru.pisarev.tm.dto.Task;

import java.util.List;

public class TaskServiceTest {

    @Nullable
    private TaskService taskService;

    @Nullable
    private Task task;

    @Before
    public void before() {
        taskService = new TaskService(new ConnectionService(new PropertyService()));
        task = taskService.add("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", new Task("Task"));
    }

    @Test
    @Category(DBCategory.class)
    public void add() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        Assert.assertNotNull(task.getName());
        Assert.assertEquals("Task", task.getName());

        @NotNull final Task taskById = taskService.findById(task.getId());
        Assert.assertNotNull(taskById);
        Assert.assertEquals(task.getId(), taskById.getId());
    }

    @Test
    @Category(DBCategory.class)
    public void findAll() {
        @NotNull final List<Task> tasks = taskService.findAll();
        Assert.assertTrue(tasks.size() > 0);
    }

    @Test
    @Category(DBCategory.class)
    public void findAllByUserId() {
        @NotNull final List<Task> tasks = taskService.findAll("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce");
        Assert.assertTrue(tasks.size() > 0);
    }

    @Test
    @Category(DBCategory.class)
    public void findAllByUserIdIncorrect() {
        @NotNull final List<Task> tasks = taskService.findAll("test");
        Assert.assertNotEquals(1, tasks.size());
    }

    @Test
    @Category(DBCategory.class)
    public void findById() {
        @NotNull final Task task = taskService.findById("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", this.task.getId());
        Assert.assertNotNull(task);
    }

    @Test
    @Category(DBCategory.class)
    public void findByIdIncorrect() {
        @NotNull final Task task = taskService.findById("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", "34");
        Assert.assertNull(task);
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyIdException.class)
    public void findByIdNull() {
        @NotNull final Task task = taskService.findById("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", null);
        Assert.assertNull(task);
    }

    @Test
    @Category(DBCategory.class)
    public void findByIdIncorrectUser() {
        @NotNull final Task task = taskService.findById("test", this.task.getId());
        Assert.assertNull(task);
    }

    @Test
    @Category(DBCategory.class)
    public void remove() {
        taskService.removeById(task.getId());
        Assert.assertNull(taskService.findById(task.getId()));
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyIdException.class)
    public void testRemoveNull() {
        taskService.removeById(null);
    }

    @Test
    @Category(DBCategory.class)
    public void findByName() {
        @NotNull final Task task = taskService.findByName("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", "Task");
        Assert.assertNotNull(task);
    }

    @Test
    @Category(DBCategory.class)
    public void findByNameIncorrect() {
        @NotNull final Task task = taskService.findByName("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", "34");
        Assert.assertNull(task);
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyNameException.class)
    public void findByNameNull() {
        @NotNull final Task task = taskService.findByName("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", null);
        Assert.assertNull(task);
    }

    @Test
    @Category(DBCategory.class)
    public void findByNameIncorrectUser() {
        @NotNull final Task task = taskService.findByName("test", this.task.getName());
        Assert.assertNull(task);
    }

    @Test
    @Category(DBCategory.class)
    public void findByIndex() {
        @NotNull final Task task = taskService.findByIndex("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", 1);
        Assert.assertNotNull(task);
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyIndexException.class)
    public void findByIndexNull() {
        @NotNull final Task task = taskService.findByIndex("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", null);
        Assert.assertNull(task);
    }

    @Test
    @Category(DBCategory.class)
    public void removeById() {
        taskService.removeById("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", task.getId());
        Assert.assertNull(taskService.findById(task.getId()));
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyIdException.class)
    public void removeByIdNull() {
        taskService.removeById("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", null);
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyIndexException.class)
    public void removeByIndexNull() {
        taskService.removeByIndex("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", null);
    }


    @Category(DBCategory.class)
    @Test(expected = EmptyNameException.class)
    public void removeByNameNull() {
        taskService.removeByName("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", null);
    }

}
