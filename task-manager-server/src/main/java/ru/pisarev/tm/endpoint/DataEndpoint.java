package ru.pisarev.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.api.service.IDataService;
import ru.pisarev.tm.api.service.ServiceLocator;
import ru.pisarev.tm.enumerated.Role;
import ru.pisarev.tm.dto.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class DataEndpoint extends AbstractEndpoint {

    @NotNull
    private IDataService dataService;

    public DataEndpoint(@NotNull ServiceLocator serviceLocator, @NotNull IDataService dataService) {
        super(serviceLocator);
        this.dataService = dataService;
    }

    @WebMethod
    @SneakyThrows
    public void loadDataBase64(@WebParam(name = "session") final Session session) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        dataService.loadDataBase64();
    }

    @WebMethod
    @SneakyThrows
    public void saveDataBase64(@WebParam(name = "session") final Session session) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        dataService.saveDataBase64();
    }

    @WebMethod
    @SneakyThrows
    public void loadDataBin(@WebParam(name = "session") final Session session) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        dataService.loadDataBin();
    }

    @WebMethod
    @SneakyThrows
    public void saveDataBin(@WebParam(name = "session") final Session session) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        dataService.saveDataBin();
    }

    @WebMethod
    @SneakyThrows
    public void loadDataJson(@WebParam(name = "session") final Session session) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        dataService.loadDataJson();
    }

    @WebMethod
    @SneakyThrows
    public void saveDataJson(@WebParam(name = "session") final Session session) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        dataService.saveDataJson();
    }

    @WebMethod
    @SneakyThrows
    public void loadDataXml(@WebParam(name = "session") final Session session) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        dataService.loadDataXml();
    }

    @WebMethod
    @SneakyThrows
    public void saveDataXml(@WebParam(name = "session") final Session session) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        dataService.saveDataXml();
    }

    @WebMethod
    @SneakyThrows
    public void loadDataYaml(@WebParam(name = "session") final Session session) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        dataService.loadDataYaml();
    }

    @WebMethod
    @SneakyThrows
    public void saveDataYaml(@WebParam(name = "session") final Session session) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        dataService.saveDataYaml();
    }

    @WebMethod
    @SneakyThrows
    public void loadDataJsonJaxB(@WebParam(name = "session") final Session session) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        dataService.loadDataJsonJaxB();
    }

    @WebMethod
    @SneakyThrows
    public void saveDataJsonJaxB(@WebParam(name = "session") final Session session) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        dataService.saveDataJsonJaxB();
    }

    @WebMethod
    @SneakyThrows
    public void loadDataXmlJaxB(@WebParam(name = "session") final Session session) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        dataService.loadDataXmlJaxB();
    }

    @WebMethod
    @SneakyThrows
    public void saveDataXmlJaxB(@WebParam(name = "session") final Session session) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        dataService.saveDataXmlJaxB();
    }
}
