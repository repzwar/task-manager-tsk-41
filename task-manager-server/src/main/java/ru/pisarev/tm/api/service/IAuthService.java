package ru.pisarev.tm.api.service;

import ru.pisarev.tm.enumerated.Role;
import ru.pisarev.tm.dto.User;

public interface IAuthService {

    String getUserId();

    User getUser();

    boolean isAuth();

    void checkRoles(Role... roles);

    void logout();

    void login(String login, String password);

    void registry(String login, String password, String email);

}
