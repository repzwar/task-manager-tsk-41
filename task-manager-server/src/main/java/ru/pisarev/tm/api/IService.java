package ru.pisarev.tm.api;

import ru.pisarev.tm.dto.AbstractEntity;

public interface IService<E extends AbstractEntity> extends IRepository<E> {

}