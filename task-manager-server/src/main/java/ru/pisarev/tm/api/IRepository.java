package ru.pisarev.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.dto.AbstractEntity;

import java.util.Collection;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    List<E> findAll();

    E add(final E entity);

    void addAll(@Nullable final Collection<E> collection);

    E findById(final String id);

    void clear();

    void removeById(final String id);

    void remove(final E entity);

}