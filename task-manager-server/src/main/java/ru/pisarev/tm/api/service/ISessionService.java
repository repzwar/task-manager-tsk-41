package ru.pisarev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.IService;
import ru.pisarev.tm.enumerated.Role;
import ru.pisarev.tm.dto.Session;
import ru.pisarev.tm.dto.User;

import java.util.List;

public interface ISessionService extends IService<Session> {

    @Nullable
    Session open(@Nullable String login, @Nullable String password);

    User checkDataAccess(@Nullable String login, @Nullable String password);

    void validate(@NotNull Session session, Role role);

    void validate(@Nullable Session session);

    @Nullable
    Session sign(@Nullable Session session);

    void close(@Nullable Session session);

    void closeAllByUserId(@Nullable String userId);

    @Nullable List<Session> findAllByUserId(@Nullable String userId);
}
